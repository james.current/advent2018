package com.jamescurrent.day1

import com.jamescurrent.util.readFile
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Day1Application : CommandLineRunner {
    override fun run(vararg args: String?) {
        val frequencyChanges = readFile(javaClass.classLoader.getResource("dayOneInput.txt").toURI()).map { it.toInt() }
        println(partOne(frequencyChanges))
        println(partTwo(frequencyChanges))
    }
}

fun main(args: Array<String>) {
    runApplication<Day1Application>(*args)
}

fun partOne(frequencyChanges: List<Int>): Int {
    return frequencyChanges.sum()
}

fun partTwo(frequencyChanges: List<Int>): Int {
    val frequencies = mutableSetOf<Int>()
    var frequency = 0

    while (true) {
        frequencyChanges.forEach { change ->
            frequency += change
            if (!frequencies.add(frequency)) {
                return frequency
            }
        }
    }
}

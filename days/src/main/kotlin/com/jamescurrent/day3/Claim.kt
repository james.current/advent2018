package com.jamescurrent.day3

data class Claim(
        val id: Int,
        val leftPad: Int,
        val topPad: Int,
        val width: Int,
        val height: Int
) {
    fun areaWithId(): List<Pair<Int, Pair<Int, Int>>> =
            (0 + leftPad until width + leftPad).flatMap { w ->
                (0 + topPad until height + topPad).map { h ->
                    Pair(id, Pair(w, h))
                }
            }
}

fun claimFromLine(line: String): Claim {
    val fields = line.split(" ")
    val id = fields[0].replace("#", "").toInt()
    val pads = fields[2].split(",").map { it.replace(":", "").toInt() }
    val dimensions = fields[3].split("x").map { it.toInt() }

    return Claim(id = id, leftPad = pads[0], topPad = pads[1], width = dimensions[0], height = dimensions[1])
}

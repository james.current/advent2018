package com.jamescurrent.day3

import com.jamescurrent.util.readFile
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Day3Application : CommandLineRunner {
    override fun run(vararg args: String?) {
        val claims = readFile(javaClass.classLoader.getResource("dayThreeInput.txt").toURI()).map { line ->
            claimFromLine(line)
        }
        println(partOne(claims))
        println(partTwo(claims))
    }
}

fun main(args: Array<String>) {
    runApplication<Day3Application>(*args)
}

//Solution found at https://www.reddit.com/r/adventofcode/comments/a2lesz/2018_day_3_solutions/eazmjae
fun partOne(claims: List<Claim>): Int {
    return claims
            .flatMap { it.areaWithId() }
            .groupingBy { it.second }
            .eachCount()
            .count { it.value > 1 }
}

//Happily I figured this one out on my own :D
fun partTwo(claims: List<Claim>): Int {
    val overlapIds = claims
            .flatMap { it.areaWithId() }
            .groupBy { it.second }
            .filter { it.value.size > 1 }
            .flatMap { it.value.map { pair -> pair.first } }
            .distinct()
    return claims
            .map { it.id }
            .minus(overlapIds)
            .first()
}

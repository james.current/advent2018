package com.jamescurrent.day4

enum class EntryType {
    GUARD, SLEEP, WAKE
}
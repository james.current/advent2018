package com.jamescurrent.day4

import com.jamescurrent.util.readFile
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Day4Application : CommandLineRunner {
    override fun run(vararg args: String?) {
        val entry = readFile(javaClass.classLoader.getResource("dayFourInput.txt").toURI()).map { line ->
            entryFromLine(line)
        }.sortedBy { it.dateTime }

//        println(partOne(entry))
//        println(partTwo(entry))
    }
}

fun main(args: Array<String>) {
    runApplication<Day4Application>(*args)
}

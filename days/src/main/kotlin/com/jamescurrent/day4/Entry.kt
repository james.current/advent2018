package com.jamescurrent.day4

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
import java.time.format.DateTimeFormatter.ISO_LOCAL_TIME
import java.time.format.DateTimeFormatterBuilder

data class Entry(
        val id: Int = -1,
        val dateTime: LocalDateTime,
        val type: EntryType
)

fun entryFromLine(line: String): Entry {
    val entryParts = line.split("]")
    val dateTimeFormatter = DateTimeFormatterBuilder()
            .append(ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .append(ISO_LOCAL_TIME)
            .toFormatter()
    val dateTime = LocalDateTime.parse(entryParts[0].replace("[", ""), dateTimeFormatter)
    val type = when(entryParts[1][2]) {
        'G' -> EntryType.GUARD
        'w' -> EntryType.WAKE
        'f' -> EntryType.SLEEP
        else -> throw IllegalStateException("unexpected format!!!")
    }
    if (type == EntryType.GUARD) {
        val id = entryParts[1].split(' ')[2].replace("#", "").toInt()
        return Entry(id = id, dateTime = dateTime, type = type)
    }
    return Entry(dateTime = dateTime, type = type)
}
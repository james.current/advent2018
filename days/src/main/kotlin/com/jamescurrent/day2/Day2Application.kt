package com.jamescurrent.day2

import com.jamescurrent.util.readFile
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Day2Application : CommandLineRunner {
    override fun run(vararg args: String?) {
        val boxIds = readFile(javaClass.classLoader.getResource("dayTwoInput.txt").toURI())
        println(partOne(boxIds))
        println(partTwo(boxIds))
    }
}

fun main(args: Array<String>) {
    runApplication<Day2Application>(*args)
}

fun partOne(ids: List<String>): Int {
    val twosAndThrees = ids.fold(Pair(0, 0)) { twoThreeTuple, id ->
        val letterOccurrences = id.toCharArray().fold(mutableMapOf<Char, Int>()) { map, char ->
            map[char] = map.getOrDefault(char, 0) + 1
            map
        }
        val two = twoThreeTuple.first +
                if (letterOccurrences.values.any { it == 2 }) 1 else 0
        val three = twoThreeTuple.second +
                if (letterOccurrences.values.any { it == 3 }) 1 else 0

        Pair(two, three)
    }
    return twosAndThrees.first * twosAndThrees.second
}

fun partTwo(ids: List<String>): String? {
    for ((i, id) in ids.withIndex()) {
        for (j in i + 1..ids.lastIndex) {
            val matches = id.zip(ids[j]).filter { pair -> pair.first == pair.second }
            if (matches.size == id.length - 1) {
                return matches.map { pair -> pair.first }.joinToString("")
            }
        }
    }
    return null
}

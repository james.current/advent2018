package com.jamescurrent.util

import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.toList

fun readFile(file: URI): List<String> {
    val path = Paths.get(file)
    val lines = Files.lines(path)
    return lines.toList()
}